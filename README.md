# Software Studio 2018 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* [Topic Name]
* Key functions (add/delete)
    1. chat
    2. load message history
    3. chat with new user
* Other functions (add/delete)
    1. notification

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|N|
|GitLab Page|5%|N|
|Database|15%|N|
|RWD|15%|N|
|Topic Key Function|15%|N|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|N|
|Chrome Notification|5%|N|
|Use CSS Animation|2.5%|N|
|Security Report|5%|N|
|Other functions|1~10%|N|

## Website Detail Description
* 在這次的midterm project中 我選擇的是製作聊天室的部分，算是學到了許多，尤其是firebase的方面。
    聊天室的相關理論與實作，是從這個網站查詢到的(https://tutorials.webduino.io/zh-tw/docs/socket/useful/im-1.html)，
    在這裡面我學到了有關聊天室等知識。舉例來說在offcanvas.js裡面的write() function就有解釋到有關訊息送出的使用者名稱(name),
    或是訊息內容(content)。

    然後老師在lab6 交給我的一些有關login logout log with google等function也幫助我省事許多，然後後來我也有在自己加一個用facebook
    登入的方式。就是利用new firebase.auth.FacebookAuthProvider()這function，並把他的資料放在facebook_provider裡面。如果登入
    成功的話就會進到window.location讓介面再度回到index.html裡面。
    不過後來我發現一個問題，就是當facebook是用gmail註冊的時候，就會產生衝突，這點需要多加注意。
    下圖是我的登入介面，有加入一個新的facebook button
    <img src="img/signin.jpg"></img>

    另外，在我的聊天室當中，還有加入了當你傳送訊息時，右下角就會有跳出你送出的訊息的提醒(就像當youtube有直播通知的時候，右下角就會有提醒一樣。
    )這個code主要是利用offcanvas.js裡面的 document.addEventListener('DOMContentLoaded', function () 以及
    notifyMe()這個function來執行的。當Notification.permission是granted的時候，就可以讓他跳出提醒以及我想要顯示的圖片
    以及字串。
    下圖代表的是右下角的通知，我可以選擇圖片，就是重要通知，以及我想要說的話。
    <img src="img/no.jpg"></img>
    
## Security Report (Optional)
{
    "rules": {
        ".read": "auth != null",
        ".write": "auth != null"
    }
}
這段規則表示如果你沒有登入的話，你就不會看到之前的聊天訊息。
